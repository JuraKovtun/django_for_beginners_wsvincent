from django.views.generic import ListView
from .models import Message


class MessageBoardView(ListView):
    model = Message
    template_name = 'message_board.html'
    context_object_name = 'all_messages_list'
