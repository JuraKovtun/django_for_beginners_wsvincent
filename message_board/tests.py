from django.test import TestCase
from django.urls import reverse
from .models import Message


class MessageBoardTest(TestCase):
    def setUp(self):
        Message.objects.create(text='some test text')

    def test_text_content(self):
        post = Message.objects.get(id=1)
        expected_text = f'{post.text}'
        self.assertEqual(expected_text, 'some test text')



