from django.urls import path
from .views import BlogView, BlogDetailView, BlogCreateView, BlogUpdateView, BlogDeleteView

urlpatterns = [
    path('', BlogView.as_view(), name='blog_view'),
    path('post/<int:pk>/', BlogDetailView.as_view(), name='post_detail_view'),
    path('post/new/', BlogCreateView.as_view(), name='post_new_view'),
    path('post/<int:pk>/edit/', BlogUpdateView.as_view(), name='post_edit_view'),
    path('post/<int:pk>/delete/', BlogDeleteView.as_view(), name='post_delete_view'),
]
