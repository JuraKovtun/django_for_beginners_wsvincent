# Chapter 1
New:
- Use pipenv to manage venv
    - `$ pip3 install pipenv`
    - `$ pipenv install Django==3.2.9`
    - `$ pipenv shell` activate venv
    - `$ exit` exit venv
- Accordingly to the [book update](https://wsvincent.com/django-for-beginners-32-update/) the author uses `python -m pip` instead of `pip` or `pipenv`, for example: `python -m pip install django`
-
    - `$ django-admin startproject [project_name] .`
    - `$ python manage.py runserver`


# Chapter 2
New:<br>
`$ sudo apt install tree`<br>
`$ python manage.py migrate`<br>
`$ python manage.py startapp [app_name]`<br>
`$ git remote add origin`<br>


# Chapter 3
New:<br>
Use a trailing slash at the end of url.  
`$ python manage.py test`<br>
No luck with Heroku, do it later.

Q:
Why do we test our home (empty string) url '' as slash '/' ?
```python
class SimpleTests(SimpleTestCase):
    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
```


# Chapter 4
New:<br>
`$ python manage.py makemigrations [app_name]`<br>
`$ python manage.py migrate`<br>
`$ python manage.py createsuperuser`<br>

# Chapter 5
New:



### Notes
- [Command Line Crash Course](https://learnpythonthehardway.org/book/appendixa.html)
- [Design philosophies](https://docs.djangoproject.com/en/dev/misc/design-philosophies/)
- [Built-in template tags and filters](https://docs.djangoproject.com/en/3.2/ref/templates/builtins/#built-in-template-tags-and-filters)
- [Function-based way to build a blog "Django Girls"](https://tutorial.djangogirls.org/en/)

